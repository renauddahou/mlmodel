import pickle


def continue_training(model, X_train, y_train):
    # Fit the model on the new training data
    model.fit(X_train, y_train)


def save_model(model):
    # Save the model to disk
    with open('./kubernetes/model_artifact/model.pkl', 'wb') as f:
        pickle.dump(model, f)


if __name__ == '__main__':
    # Load the model from disk
    with open('./kubernetes/model_artifact/model.pkl', 'rb') as f:
        model = pickle.load(f)

    # Load the new training data
    X_train = ...
    y_train = ...

    # Continue training the model
    continue_training(model, X_train, y_train)

    # Save the updated model to disk
    save_model(model)
