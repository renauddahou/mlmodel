import pickle
from boxkite.monitoring.collector import BaselineMetricCollector
from boxkite.monitoring.service import ModelMonitoringService
# Flask Imports
from flask import render_template
from flask import Flask, jsonify, request, make_response
from prometheus_client import generate_latest
from prometheus_client import Counter, Histogram
import time
import json
import numbers
import ast
# Counter and Histogram are examples of default metrics
# available from the prometheus Python client.


def response(code=None, message=None, data=[]):
    """
    Définition d'un format uniforme pour les paramètres de retour
    """
    if code is None:
        code = 10200
    if message is None:
        message = "success"

    return jsonify({"code": code, "message": message, "data": data})


REQUEST_COUNT = Counter(
    name='http_request_count',
    documentation='App Request Count',
    labelnames=['app_name', 'method', 'endpoint', 'http_status'])
REQUEST_LATENCY = Histogram(name='http_request_latency_seconds',
                            documentation='Request latency',
                            labelnames=['app_name', 'endpoint'])

with open("./model.pkl", "rb") as f:
    model = pickle.load(f)

monitor = ModelMonitoringService(baseline_collector=BaselineMetricCollector(
    path="./histogram.prom"))

app = Flask(__name__)


# check if server is running
@app.route('/test', methods=['GET'])
def test_method():
    return make_response(jsonify({'msg': 'Server running'}), 200)


# error
@app.route('/error', methods=['GET'])
def error_method():
    return make_response(jsonify({'msg': 'Error'}), 500)


@app.before_request
def start_timer():
    """Get start time of a request."""
    request._prometheus_metrics_request_start_time = time.time()


@app.after_request
def stop_timer(response):
    """Get stop time of a request.."""
    request_latency = time.time(
    ) - request._prometheus_metrics_request_start_time
    REQUEST_LATENCY.labels(app_name='web',
                           endpoint=request.path).observe(request_latency)
    return response


@app.after_request
def record_request_data(response):
    """Capture request data.

    Uses the flask request object to extract information such as
    the HTTP request method, endpoint and HTTP status.
    """
    REQUEST_COUNT.labels(app_name='web',
                         method=request.method,
                         endpoint=request.path,
                         http_status=response.status_code).inc()
    return response


@app.route("/predict", methods=["POST"])
def predict():
    if request.method == "POST":
        try:
            data = json.loads(request.get_data())
        except json.decoder.JSONDecodeError:
            return response(10105, "format error")

        try:
            sepal_length = data["sepal length (cm)"]
            sepal_width = data["sepal width (cm)"]
            petal_length = data["petal length (cm)"]
            petal_width = data["petal width (cm)"]
        except KeyError:
            return response(10102, "key null")

        else:
            if sepal_length == "":
                return response(10103, "sepal_length null")
            elif sepal_width == "":
                return response(10103, "sepal_width null")
            elif petal_length == "":
                return response(10103, "petal_length null")
            elif petal_width == "":
                return response(10103, "petal_width null")
            else:
                array = {
                    "sepal length (cm)": sepal_length,
                    "sepal width (cm)": sepal_width,
                    "petal length (cm)": petal_length,
                    "petal width (cm)": petal_width
                }
                if isinstance(array['sepal length (cm)'], numbers.Real
                              ) is False or array['sepal length (cm)'] < 0:
                    return response(10101,
                                    "sepal_length is not real positif number")

                elif isinstance(array['sepal width (cm)'], numbers.Real
                                ) is False or array['sepal width (cm)'] < 0:
                    return response(10101,
                                    "sepal_width is not real positif number")

                elif isinstance(array['petal length (cm)'], numbers.Real
                                ) is False or array['petal length (cm)'] < 0:
                    return response(10101,
                                    "petal_length is not real positif number")

                elif isinstance(array['petal width (cm)'], numbers.Real
                                ) is False or array['petal width (cm)'] < 0:
                    return response(10101,
                                    "petal_width is not real positif number")

                else:
                    features = list(array.values())  # json to list
                    score = model.predict([features])[0]
                    pid = monitor.log_prediction(
                        request_body=request.data,
                        features=features,
                        output=score,
                    )
                    data = {"result": score, "prediction_id": pid}
                    return response(10200, "get success", data)
    else:
        return response(10101, "request method error")


@app.route('/')
def home():
    return render_template('index.html')


@app.route("/predict_api", methods=["POST"])
def predict_api():
    if request.method == "POST":
        try:
            sepal_length = ast.literal_eval(request.form["sepal length (cm)"])
            sepal_width = ast.literal_eval(request.form["sepal width (cm)"])
            petal_length = ast.literal_eval(request.form["petal length (cm)"])
            petal_width = ast.literal_eval(request.form["petal width (cm)"])
        except KeyError:
            output = "key is null"
            return render_template('index.html',
                                   error_text='error {}'.format(output))

        else:
            if sepal_length == "":
                output = "sepal_length is null"
                return render_template('index.html',
                                       error_text='error {}'.format(output))
            elif sepal_width == "":
                output = "sepal_width is null"
                return render_template('index.html',
                                       error_text='error {}'.format(output))
            elif petal_length == "":
                output = "petal_length is  null"
                return render_template('index.html',
                                       error_text='error {}'.format(output))
            elif petal_width == "":
                output = "petal_length is  null"
                return render_template('index.html',
                                       error_text='error {}'.format(output))
            else:
                array = {
                    "sepal length (cm)": sepal_length,
                    "sepal width (cm)": sepal_width,
                    "petal length (cm)": petal_length,
                    "petal width (cm)": petal_width
                }

                if isinstance(array['sepal length (cm)'], numbers.Real
                              ) is False or array['sepal length (cm)'] < 0:
                    output = "sepal_length is not real positif number"
                    return render_template(
                        'index.html', error_text='error {}'.format(output))

                elif isinstance(array['sepal width (cm)'], numbers.Real
                                ) is False or array['sepal width (cm)'] < 0:
                    output = "sepal_width is not real positif number"
                    return render_template(
                        'index.html', error_text='error {}'.format(output))

                elif isinstance(array['petal length (cm)'], numbers.Real
                                ) is False or array['petal length (cm)'] < 0:
                    output = "petal_length is not real positif number"
                    return render_template(
                        'index.html', error_text='error {}'.format(output))

                elif isinstance(array['petal width (cm)'], numbers.Real
                                ) is False or array['petal width (cm)'] < 0:
                    output = "petal_width is not real positif number"
                    return render_template(
                        'index.html', error_text='error {}'.format(output))

                else:
                    features = list(array.values())  # json to list
                    score = model.predict([features])[0]
                    pid = monitor.log_prediction(
                        request_body=request.data,
                        features=features,
                        output=score,
                    )
                    data = {"result": score, "prediction_id": pid}
                    output = data["result"]
                    return render_template(
                        'index.html',
                        prediction_text='The prediction is  {}'.format(output))
    else:
        return response(10101, "request method error")


"""
@app.route("/metrics", methods=["GET"])
def metrics():
    return monitor.export_http()[0]
"""


@app.route('/metrics')
def metrics():
    return monitor.export_http()[0] + generate_latest()


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=False)
