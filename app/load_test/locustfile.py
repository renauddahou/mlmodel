"""
Run load tests:

locust -f load_test/locustfile.py --host http://localhost:5000
"""

from locust import HttpUser, task
import pandas as pd
import numpy as np
from sklearn.datasets import load_iris

iris = load_iris()
dataset1 = pd.DataFrame(data=np.c_[iris['data']],
                        columns=iris['feature_names'])

driftdata = dataset1 + 5
dataset = pd.read_csv('file.csv')

# iris = load_iris()
# dataset = pd.DataFrame(data= np.c_[iris['data']],
# columns= iris['feature_names'])

# driftdata = dataset+5

dataset = pd.read_csv('file.csv')


# Define locust class
class IrisPredictionUser(HttpUser):

    @task(3)
    def test(self):
        self.client.get("/test")

    @task(92)
    def prediction(self):
        record = dataset.sample(n=1).copy()
        input_for_pred = record.to_dict(orient='records')[0]
        self.client.post("/predict", json=input_for_pred)

    @task(5)
    def prediction_bad_value(self):
        input_for_pred = ["bad data"]
        self.client.post("/predict", json=input_for_pred)
