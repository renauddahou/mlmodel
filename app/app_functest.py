# import sys
import unittest
import warnings
# import time

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('--disable-blink-features=AutomationControlled')
webdriver_service = Service("/chromedriver/stable/chromedriver")


class TestAPI(unittest.TestCase):

    def setUp(self):
        warnings.simplefilter("ignore", ResourceWarning)
        self.driver = webdriver.Chrome(service=webdriver_service,
                                       options=chrome_options)
        self.base_url = 'http://localhost:5000'

    def tearDown(self):
        self.driver.close()

    def test_form_submission_succes(self):
        # start_time = time.time()
        self.driver.get(self.base_url)
        self.driver.find_element(
            By.CSS_SELECTOR,
            "body > div.login > form > input[type=number]:nth-child(1)"
        ).send_keys(5.9)
        self.driver.find_element(
            By.CSS_SELECTOR,
            "body > div.login > form > input[type=number]:nth-child(2)"
        ).send_keys(3.0)
        self.driver.find_element(
            By.CSS_SELECTOR,
            "body > div.login > form > input[type=number]:nth-child(3)"
        ).send_keys(5.1)
        self.driver.find_element(
            By.CSS_SELECTOR,
            "body > div.login > form > input[type=number]:nth-child(4)"
        ).send_keys(1.8)
        self.driver.find_element(By.CSS_SELECTOR,
                                 "body > div.login > form > button").click()
        self.assertEqual(
            self.driver.find_element(
                By.CSS_SELECTOR, "body > div.login > h3:nth-child(5)").text,
            'The prediction is virginica')
        """
        elapsed_time = time.time() - start_time
        if elapsed_time > 10:
            print("Page took too long to load!")
            print(elapsed_time)
            sys.exit(1)
        else:
            print("Page loaded in acceptable time.")
            print(elapsed_time)
        """

    def test_form_submission_error(self):
        # start_time = time.time()
        self.driver.get(self.base_url)
        self.driver.find_element(
            By.CSS_SELECTOR,
            "body > div.login > form > input[type=number]:nth-child(1)"
        ).send_keys(-5.9)
        self.driver.find_element(
            By.CSS_SELECTOR,
            "body > div.login > form > input[type=number]:nth-child(2)"
        ).send_keys(3.0)
        self.driver.find_element(
            By.CSS_SELECTOR,
            "body > div.login > form > input[type=number]:nth-child(3)"
        ).send_keys(5.1)
        self.driver.find_element(
            By.CSS_SELECTOR,
            "body > div.login > form > input[type=number]:nth-child(4)"
        ).send_keys(1.8)
        self.driver.find_element(By.CSS_SELECTOR,
                                 "body > div.login > form > button").click()
        self.assertEqual(
            self.driver.find_element(
                By.CSS_SELECTOR, "body > div.login > h3:nth-child(6)").text,
            'error sepal_length is not real positif number')
        """
        elapsed_time = time.time() - start_time
        if elapsed_time > 10:
            print("Page took too long to load!")
            print(elapsed_time)
            sys.exit(1)
        else:
            print("Page loaded in acceptable time.")
            print(elapsed_time)
        """


if __name__ == '__main__':
    unittest.main()
