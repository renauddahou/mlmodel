import unittest
import warnings
from flask_app import app


class BaseCase(unittest.TestCase):

    def setUp(self):
        warnings.simplefilter("ignore", ResourceWarning)
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()

    def tearDown(self):
        self.ctx.pop()


class sampleTest(BaseCase):

    def test_sample(self):
        r = self.client.get('/test')
        assert (r.status_code == 200)
        result = r.json
        print(result)


class POSTJsonTest(BaseCase):

    def test_key_null(self):
        payload = {}
        r = self.client.post("/predict", json=payload)
        print(r)
        result = r.json
        print(result)

    def test_sepal_length_null(self):
        payload = {
            'sepal length (cm)': "",
            'sepal width (cm)': 3.5,
            'petal length (cm)': 1.4,
            'petal width (cm)': 0.2
        }
        r = self.client.post("/predict", json=payload)
        result = r.json
        print(result)

    def test_sepal_type(self):
        payload = {
            'sepal length (cm)': "5.1",
            'sepal width (cm)': 3.5,
            'petal length (cm)': 1.4,
            'petal width (cm)': 0.2
        }
        r = self.client.post("/predict", json=payload)
        result = r.json
        print(result)

    def test_add_success(self):
        payload = {
            'sepal length (cm)': 5.1,
            'sepal width (cm)': 3.5,
            'petal length (cm)': 1.4,
            'petal width (cm)': 0.2
        }
        r = self.client.post("/predict", json=payload)
        result = r.json
        print(result)


if __name__ == "__main__":
    unittest.main()
