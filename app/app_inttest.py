import unittest
import warnings
import requests


class BaseCase(unittest.TestCase):

    def setUp(self):
        warnings.simplefilter("ignore", ResourceWarning)
        self.base_url = "http://127.0.0.1:5000"


class sampleTest(BaseCase):

    def test_sample(self):
        r = requests.get(self.base_url + "/test", verify=False)
        result = r.json()
        print(result)


class POSTJsonTest(BaseCase):

    def test_key_null(self):
        payload = {}
        r = requests.post(self.base_url + "/predict",
                          json=payload,
                          verify=False)
        print(r)
        result = r.json()
        print(result)

    def test_sepal_length_null(self):
        payload = {
            'sepal length (cm)': "",
            'sepal width (cm)': 3.5,
            'petal length (cm)': 1.4,
            'petal width (cm)': 0.2
        }
        r = requests.post(self.base_url + "/predict",
                          json=payload,
                          verify=False)
        result = r.json()
        print(result)

    def test_sepal_type(self):
        payload = {
            'sepal length (cm)': "5.1",
            'sepal width (cm)': 3.5,
            'petal length (cm)': 1.4,
            'petal width (cm)': 0.2
        }
        r = requests.post(self.base_url + "/predict",
                          json=payload,
                          verify=False)
        result = r.json()
        print(result)

    def test_add_success(self):
        payload = {
            'sepal length (cm)': 5.1,
            'sepal width (cm)': 3.5,
            'petal length (cm)': 1.4,
            'petal width (cm)': 0.2
        }
        r = requests.post(self.base_url + "/predict",
                          json=payload,
                          verify=False)
        result = r.json()
        print(result)


if __name__ == "__main__":
    unittest.main()
