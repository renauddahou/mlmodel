#!/bin/bash

# Define the old word to be replaced
old_word=$1

# Define the new word to replace the old word
new_word=$2

# Define the file to be modified
file=$3

# Use sed to perform the replacement
sed "s~${old_word}~${new_word}~g" $file > "${file}.tmp" && mv "${file}.tmp" $file