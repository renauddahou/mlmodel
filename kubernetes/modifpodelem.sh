#!/bin/bash

app_label=$1
metrics_path=$2
destination_path=$3

pod_names=($(kubectl get pods --selector=app=$app_label --output=jsonpath={.items[*].metadata.name}))
for pod_name in "${pod_names[@]}"; do
  kubectl cp $metrics_path default/$pod_name:$destination_path
done